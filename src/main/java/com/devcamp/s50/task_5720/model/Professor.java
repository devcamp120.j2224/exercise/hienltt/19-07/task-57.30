package com.devcamp.s50.task_5720.model;

import java.util.ArrayList;

public class Professor extends Person {
    private int salary;

    public Professor(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public Professor(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }
    
    public Professor(int age, String gender, String name, Address address, int salary, ArrayList<Animals> listPet) {
        super(age, gender, name, address, listPet);
        this.salary = salary;
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Professor is eating...");
    }

    public void teaching(){
        System.out.println("Professor is teaching....");
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
