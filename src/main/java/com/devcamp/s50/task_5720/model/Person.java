package com.devcamp.s50.task_5720.model;

import java.util.ArrayList;

public abstract class Person {
    private int age;
    private String gender;
    private String name;
    private Address address;
    private ArrayList<Animals> listPet;

    public abstract void eat();

    public Person(int age, String gender, String name, Address address, ArrayList<Animals> listPet) {
        this.age = age;
        this.gender = gender;
        this.name = name;
        this.address = address;
        this.listPet = listPet;
    }

    public Person(int age, String gender, String name, Address address) {
        this.age = age;
        this.gender = gender;
        this.name = name;
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<Animals> getListPet() {
        return listPet;
    }

    public void setListPet(ArrayList<Animals> listPet) {
        this.listPet = listPet;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
