package com.devcamp.s50.task_5720.model;

import java.util.ArrayList;

public class Worker extends Person {
    
    private int salary; 

    public Worker(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
        //TODO Auto-generated constructor stub
    }

    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public Worker(int age, String gender, String name, Address address, int salary , ArrayList<Animals> listPet) {
        super(age, gender, name, address, listPet);
        this.salary = salary;
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Worker is eating...");
    }
    
    public String working(){
        return new String("Worker is working...");
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
