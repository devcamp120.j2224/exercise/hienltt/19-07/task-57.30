package com.devcamp.s50.task_5720.model;

public class Animals {
    private String animalType;
    private String animalName;

    public Animals() {
        this.animalName = "Nickky";
        this.animalType = "Dog";
    }    

    public Animals(String animalType, String animalName) {
        this.animalType = animalType;
        this.animalName = animalName;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public boolean isMammal() {
        return true;
    };

    public void mate() {
        System.out.println("animal mating ...");
    }
}
