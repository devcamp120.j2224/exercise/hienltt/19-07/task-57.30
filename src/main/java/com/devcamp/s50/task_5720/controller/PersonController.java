package com.devcamp.s50.task_5720.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task_5720.model.*;

@RestController
public class PersonController {
    // Tạo API có tên listStudent trả ra danh sách sinh viên
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent() {
        ArrayList<Student> students = new ArrayList<Student>();

        Subject subjectHoadaicuong = new Subject("Hoá Đại Cương", 1, new Professor(50, "male", "Tom Cruise", new Address())); 
        ArrayList<Subject> subject01s = new ArrayList<>();
        subject01s.add(subjectHoadaicuong);

        Student studentPeter = new Student(18, "male", "Peter", new Address(), 1, subject01s);
        Student studentMarry = new Student(19, "female", "Marry", new Address(), 2,
                    new ArrayList<Subject>() {
                    {
                        add(new Subject("Vật lý đại cương", 2, new Professor(50, "male", "Tom Cruise", new Address())));
                        add(new Subject("Hình học", 3, new Professor(55, "female", "Madona", new Address())));  
                    }
        });
        
        Student studentDiana = new Student(20, "female", "Diana", new Address(), 3,
                    new ArrayList<Subject>() {
                    {
                        add(new Subject("Anh Văn", 4, new Professor(50, "male", "Tom Cruise", new Address())));
                        add(new Subject("Giải tích", 5, new Professor(55, "female", "Madona", new Address())));  
                    }
        });

        students.add(studentPeter);
        students.add(studentMarry);
        students.add(studentDiana);
        return students;
    }

    @GetMapping("/listPerson")
    public ArrayList<Person> getListPerson(@RequestParam(defaultValue = "0") Integer type){
        ArrayList<Person> person = new ArrayList<Person>();

        Student studentA = new Student(18, "male", "Peter", new Address(), 1,
                new ArrayList<Subject>() {
                    {
                        add(new Subject("Hoá hữu cơ", 6, new Professor(45, "male", "Tony", new Address())));
                    }
                },
                new ArrayList<Animals>() {
                    {
                        add(new Animals("Dog", "LuLu"));
                    }
                }
        );
        Student studentB = new Student(19, "female", "Sahra", new Address(), 2,
                new ArrayList<Subject>() {
                    {
                        add(new Subject("Cơ học lượng tử", 7, new Professor(51, "male", "Einstein", new Address())));
                    }
                },
                new ArrayList<Animals>() {
                    {
                        add(new Animals("Cat", "Mickey"));
                    }
                }
        );

        Worker worker1 = new Worker(28, "male", "Athony", new Address(), 15000000,
                    new ArrayList<Animals>() {
                        {
                            add(new Animals());
                        }
                    }
        );
        Worker worker2 = new Worker(30, "female", "Nebula", new Address(), 18000000,
                    new ArrayList<Animals>(){
                        {
                            add(new Animals("Hamster", "Pom"));
                        }
                    }
        );


        Professor professorI = new Professor(45, "male", "Tony", new Address(), 30000000,
            new ArrayList<Animals>() {
                {
                    add(new Animals());
                }
            }
        );
        Professor professorII = new Professor(51, "male", "Einstein", new Address(), 35000000,
            new ArrayList<Animals>(){
                {
                    add( new Animals("Dog", "Max"));
                }
            }
        );

        switch (type) {
            case 1: 
                person.add(studentA);
                person.add(studentB);
                return person;
            case 2: 
                person.add(worker1);
                person.add(worker2);
                return person;
            case 3: 
                person.add(professorI);
                person.add(professorII);
                return person;
            default:
                person.add(studentA);
                person.add(studentB);
                person.add(worker1);
                person.add(worker2);
                person.add(professorI);
                person.add(professorII);
                return person;
        }      

        
        // if (type == 1){
        //     person.add(studentA);
        //     person.add(studentB);
        //     return person;
        // } else if (type == 2){
        //     person.add(worker1);
        //     person.add(worker2);
        //     return person;
        // } else if (type == 3){
        //     person.add(professorI);
        //     person.add(professorII);
        //     return person;            
        // } else{
        //     person.add(studentA);
        //     person.add(studentB);
        //     person.add(worker1);
        //     person.add(worker2);
        //     person.add(professorI);
        //     person.add(professorII);
        //     return person;            
        // }
    }
}
